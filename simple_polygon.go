package go_crate_types

import (
	"database/sql/driver"
	"fmt"
	"strings"
)

type Simple_Polygon struct {
	Points []*Geo_point
}

func (s *Simple_Polygon) String() string {
	var res = "POLYGON(("
	var tmp = "%.6f %.6f,"
	for _, point := range s.Points {
		res += fmt.Sprintf(tmp, point.Lng, point.Lat)
	}

	res = strings.TrimSuffix(res, ",")
	res += "))"
	return res
}

func (s *Simple_Polygon) Scan(val interface{}) error {
	return fmt.Errorf("Scan for Simple_Polygon not supported yet")
}

func (s *Simple_Polygon) Value() (driver.Value, error) {
	return s.String(), nil
}

func (s *Simple_Polygon) Centroid() *Geo_point {
	adaptedLen := (len(s.Points) - 1) // s.Points[0] == s.Points[len(s.Points)]
	lat := 0.
	lng := 0.
	for i := 0; i < adaptedLen; i++ {
		lat += s.Points[i].Lat
		lng += s.Points[i].Lng
	}

	return &Geo_point{
		Lat: lat / float64(adaptedLen),
		Lng: lng / float64(adaptedLen),
	}
}
