package go_crate_types

import (
	"encoding/json"
	"strings"
	"testing"
)

func TestGeo_point_String(t *testing.T) {
	gp := &Geo_point{
		Lat: 2.443,
		Lng: 5.214,
	}
	expected := "POINT(5.214000 2.443000)"
	if strings.Compare(gp.String(), expected) != 0 {
		t.Error(
			"For (2.433, 5.214) ",
			"expected ", expected,
			" got ", gp.String(),
		)
	}
}

func TestGeo_point_Scan(t *testing.T) {
	var in []json.Number
	in = append(in, json.Number("2.443"))
	in = append(in, json.Number("5.214"))

	gp := &Geo_point{}
	if err := gp.Scan(in); err != nil {
		t.Error(err)
	}

	if gp.Lng != 2.443 || gp.Lat != 5.214 {
		t.Error(
			"Expected Lng: 2.443, Lat: 5.214 ",
			" Got Lat: ", gp.Lat,
			" Lng: ", gp.Lng,
		)
	}
}

func TestGeo_point_Value(t *testing.T) {
	gp := &Geo_point{
		Lat: 2.443,
		Lng: 5.214,
	}
	val, err := gp.Value()
	expected := "POINT(5.214000 2.443000)"
	if err != nil || strings.Compare(val.(string), expected) != 0 {
		t.Error(
			"Expected non nil error and ", expected,
			" Got ", err,
			" and ", val,
		)
	}
}
