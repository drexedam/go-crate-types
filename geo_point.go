package go_crate_types

import (
	"database/sql/driver"

	"encoding/json"

	"fmt"
)

type Geo_point struct {
	Lat float64

	Lng float64
}

func (g *Geo_point) String() string {

	return fmt.Sprintf("POINT(%.6f %.6f)", g.Lng, g.Lat)

}

func (g *Geo_point) Scan(val interface{}) error {

	slc, err := interfaceSlice(val)

	if err != nil {

		return err

	}

	if len(slc) != 2 {

		return fmt.Errorf("invalid number of entries")

	}

	latRaw := slc[1]

	lngRaw := slc[0]

	if ok, v := isJsonNumber(latRaw); !ok {

		return fmt.Errorf("invalid type %T for latitude", v)

	}

	if ok, v := isJsonNumber(lngRaw); !ok {

		return fmt.Errorf("invalid type %T for longitude", v)

	}

	lat, err := latRaw.(json.Number).Float64()

	if err != nil {

		return err

	}

	lng, err := lngRaw.(json.Number).Float64()

	if err != nil {

		return err

	}

	g.Lat = lat

	g.Lng = lng

	return nil

}

func (g *Geo_point) Value() (driver.Value, error) {

	return g.String(), nil

}
