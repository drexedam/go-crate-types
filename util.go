package go_crate_types

import (
	"encoding/json"
	"fmt"
	"reflect"
)

func interfaceSlice(slice interface{}) ([]interface{}, error) {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		return nil, fmt.Errorf("InterfaceSlice() given a non-slice type")
	}

	ret := make([]interface{}, s.Len())

	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret, nil
}

func isJsonNumber(val interface{}) (bool, interface{}) {

	switch v := val.(type) {
	case json.Number:
		return true, v
	default:
		return false, v
	}

}
