go-crate-types
========

Additional go data type for Crate. (https://crate.io/)

Install & Usage
--------
```
go get bitbucket.org/drexedam/go-crate-types
```

```golang
import gct "bitbucket.org/drexedam/go-crate-types"

type Event struct {
	Time_Stamp	    *gct.Time
	Location        *gct.Geo_point
}

ev := &Event{}
// Could be that you have to add an empty Geo_Point: ev.Location = &Geo_point{}
err = rows.StructScan(ev)
if err != nil {
	// Handle error
}
```