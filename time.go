package go_crate_types

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

type Time struct {
	TimeStamp time.Time
}

func (t *Time) String() string {
	return strconv.FormatInt(t.TimeStamp.Unix(), 10)
}

func (t *Time) Scan(val interface{}) error {
	if ok, v := isJsonNumber(val); !ok {
		return fmt.Errorf("invalid type %T", v)
	}

	num, err := val.(json.Number).Int64()
	if err != nil {
		return err
	}
	t.TimeStamp = time.Unix(num, 0)

	return nil
}

func (t *Time) Value() (driver.Value, error) {
	return t.String(), nil
}
